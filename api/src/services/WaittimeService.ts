import { Park, SimpleTimeseries, SimpleWaitTime, WaitRecord } from "model/models";
import { singleton } from "tsyringe";

@singleton()
export class WaittimeService {

    private cachedClients = {};

    constructor() {}

    async getAllParks(ridesOnly=false, time=0) {
        let acc: WaitRecord[] = [];
        const all: WaitRecord[][] = await Promise.all(
            Object.values(Park).map(async (park: Park) =>
                await this.getCurrentWaitTimes(park, ridesOnly, time)
            )
        );
        all.forEach((list: WaitRecord[]) => acc = acc.concat(list));
        return acc;
    }

    async getAllRideTimes(starttime, endtime, period) {
        const allTimestamps: number[] = [];
        for (let i = starttime; i < endtime; i = i + period) {
            allTimestamps.push(i);
        }
        let all = await Promise.all(
            allTimestamps.map(async ts => await this.getAllParks(true, ts))
        );
        let all1d: WaitRecord[] = [];
        for (let row of all) for (let e of row) all1d.push(e);
        all1d = all1d.filter(x => x.ride.indexOf("Unavailable") === -1)
        let results = {};
        all1d.forEach(async x => {
            if (!results.hasOwnProperty(x.ride)) {
                let rideTS = new SimpleTimeseries();
                rideTS.type = x.type;
                rideTS.park = x.park;
                rideTS.ride = x.ride;
                rideTS.data = [];
                results[x.ride] = rideTS;
            }
            let tsRecord = new SimpleWaitTime();
            tsRecord.status = x.status;
            tsRecord.timestamp = x.timestamp;
            tsRecord.waittime = x.waittime;
            results[x.ride].data.push(tsRecord);
        })
        return results;
    }

    async getCurrentWaitTimes(park: Park, ridesOnly=false, time=0): Promise<WaitRecord[]> {
        const wrapper = this.getParkWrapper(park);
        try {
            const timestamp = time === 0 ? Date.now() : time;
            const waits = await wrapper.GetWaitTimes();
            const standard: WaitRecord[] = waits.map((wait: any) => {
                return this.standardizeResponse(park, wait, timestamp);
            });
            const filtered = standard.filter((ride: WaitRecord) =>
                !ridesOnly || ride.type === "ATTRACTION"
            )
            return filtered;
        } catch (e) {
            return [];
        }
        return [];
    }

    private standardizeResponse(park: Park, wait: any, date?: number): WaitRecord {
        const record: WaitRecord = new WaitRecord();
        record.id = wait.id;
        record.park = park;
        record.ride = wait.name;
        record.status = wait.status;
        record.type = wait.meta.type;
        record.waittime = !wait.active ? -1 : wait.waitTime;
        record.timestamp = date ?? Date.now();
        return record;
    }

    private getParkWrapper(park: Park) {
        const Themeparks = require("themeparks");
        switch(park) {
            case Park.MAGIC_KINGDOM:
                if (!(park in this.cachedClients)) {
                    this.cachedClients[park] = new Themeparks.Parks.WaltDisneyWorldMagicKingdom();
                }
                return this.cachedClients[park];
            case Park.ANIMAL_KINGDOM:
                if (!(park in this.cachedClients)) {
                    this.cachedClients[park] = new Themeparks.Parks.WaltDisneyWorldAnimalKingdom();
                }
                return this.cachedClients[park];
            case Park.HOLLYWOOD_STUDIOS:
                if (!(park in this.cachedClients)) {
                    this.cachedClients[park] = new Themeparks.Parks.WaltDisneyWorldHollywoodStudios();
                }
                return this.cachedClients[park];
            case Park.EPCOT:
                if (!(park in this.cachedClients)) {
                    this.cachedClients[park] = new Themeparks.Parks.WaltDisneyWorldEpcot();
                }
                return this.cachedClients[park];
            default:
                throw new Error("Park not found");
        }
    }

}