'use strict'
import 'app-module-path/register';
import 'reflect-metadata';
import './registry/Registry';
const awsServerlessExpress = require('aws-serverless-express');
const app = require('./app');
const server = awsServerlessExpress.createServer(app);

exports.handler = (event, context) => awsServerlessExpress.proxy(server, event, context);