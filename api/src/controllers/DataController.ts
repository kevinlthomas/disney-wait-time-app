import { Get, JsonController, OnNull, QueryParam } from "routing-controllers";
import { WaittimeService } from "services/WaittimeService";
import { injectable } from "tsyringe";

@injectable()
@JsonController("/data")
export default class DataController {

    private waittimeService: WaittimeService;

    constructor(
        waittimeService: WaittimeService
    ) {
        this.waittimeService = waittimeService;
    }

    @Get("/ping")
    async ping() {
        return { "hello": "world" }
    }

    @Get("/waittimes")
    @OnNull(500)
    async getTimes(
        @QueryParam("start") start: number,
        @QueryParam("end") end: number,
        @QueryParam("period") period: number
    ) {
        let defaultPeriod = 30*60*1000; // default 30 minutes
        let defaultRange = 1000; //default 1 second (i.e. instant)
        let endtime = end == null ? Date.now() : end;
        let starttime = start == null ? endtime-defaultRange : start;
        let periodTime = period == null ? defaultPeriod  : period * 60 * 1000;
        let data = await this.waittimeService.getAllRideTimes(
            starttime, endtime, periodTime
        );
        return {
            data: data
        };
    }

}