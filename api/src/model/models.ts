export class WaitRecord {
    id!: string;
    park!: string;
    ride!: string;
    waittime!: number;
    status!: string;
    type!: string;
    timestamp!: number;
}
export enum Park {
    MAGIC_KINGDOM = "Magic Kingdom",
    ANIMAL_KINGDOM = "Animal Kingdom",
    HOLLYWOOD_STUDIOS = "Hollywood Studios",
    EPCOT = "Epcot"
}
export class SimpleTimeseries {
    park!: string;
    ride!: string;
    type!: string;
    data!: SimpleWaitTime[];
}
export class SimpleWaitTime {
    timestamp!: number;
    status!: string;
    waittime!: number;
}