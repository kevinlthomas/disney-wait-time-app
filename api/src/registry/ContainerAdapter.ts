import { Action, ClassConstructor, IocAdapter } from 'routing-controllers';
import { container } from 'tsyringe';
 
export class ContainerAdapter implements IocAdapter {

    constructor (
    ) {
    }
    
    get<T> (someClass: ClassConstructor<T>, action?: Action): T {
        return container.resolve(someClass)
    } 
}