'use strict'
import "reflect-metadata"
import dotenv from 'dotenv';


import express, { Application, NextFunction } from 'express';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser'
import { IocAdapter, useContainer, useExpressServer } from "routing-controllers";
import { Request, Response } from "express";
import { ContainerAdapter } from "registry/ContainerAdapter";
import DataController from "controllers/DataController";

dotenv.config();

var app: Application = express();
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// LOG ALL REQUESTS
app.use(function(req: Request, res: Response, next: NextFunction) {
    console.log(req.url);
    next();
});

const containerAdapter: IocAdapter = new ContainerAdapter();
useContainer(containerAdapter);
useExpressServer(app, {
    cors: { 
        origin: true,
        credentials: true
    },
    controllers: [
        DataController
    ] 
});

module.exports = app;