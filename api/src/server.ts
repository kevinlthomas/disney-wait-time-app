require('dotenv').config({path: __dirname + '/../.env'});
import 'app-module-path/register';
import "reflect-metadata";
const app = require('./app');

const PORT = process.env.PORT ?? 3001;

app.listen(PORT, () => console.log(`Listening on port ${PORT}`));